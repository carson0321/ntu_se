# NTU_SE

National Taiwan University 2014 Software Engineering Course

# Project

*	Name:TranOn(Team 4)  
*	Member: Uiling、Ray、Sandy、Mars、Hsuan-chen 、Carson、Chih-hsien、Lan
*	Programming Language: Java (Including Android app)
*	IDE: Android Studio, Eclipse

# Introduction

The hearing-loss people have the effects of deprivation of information. The situation is like moving to a country of other languages. They have same common points that can hear the voices clearly, but don’t understand the contents of voice. We want to develop an app with the cell phone. It can improve this situation.

# Requirement

* For others: The app will turn on microphone and translate what they said.
* For hearing-impaired people: It will display the sentences the other people said.

# Main function

Use the cell phone as a microphone. We can make the app receive the voices to handle. Finally, it can display the information which is the contents of voice in the cell phone’s screen.

### Types of meeting:
*	Public Meeting: Can see meeting in location list and don’t need the permission from admin.
*	Private Meeting: Can see meeting in location list but need the permission from admin.
*	Hidden meeting: Can’t see meeting in location list but don’t need the permission from admin.
*	Secret meeting: Can’t see meeting in location list and need the permission from admin to get into meeting.

### Participant: 
*	General participant.
### Initiator:
*	Participant who initiate the meeting is initiator. Initiator will be the first administrator in the meeting.
### Administrator:
*	Control the meeting.

Can change view permission of meeting history  
Unrestricted: Don’t need permission to view the history.  
Restricted: Need permission to view the history.


# Possible difficulty

1. According to the amount of recognition vocabulary :( may need a lot of data)  
Small amount (hundred), Big amount (thousand)
  
2. Interfering factors:    
Speak Speed, Sex, Personal Habit, Language, Voice Continuity, Voice Recognition